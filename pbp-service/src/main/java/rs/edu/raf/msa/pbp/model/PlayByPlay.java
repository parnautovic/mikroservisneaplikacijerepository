package rs.edu.raf.msa.pbp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class PlayByPlay {

	Map<String, Player> players = new LinkedHashMap<>();
	
	List<Quarter> quarters = new ArrayList<>(4);

	public List<Play> play(String fromMin, String toMin) {
		// TODO Returns all plays by all quarters whose
		List<Play> plays = new ArrayList<Play>();
		for(Quarter q : quarters){
			Integer minAdd = 0;
			if(q.q == 1) minAdd = 0;
			else if(q.q == 2) minAdd += 12 * 60;
			else if(q.q == 3) minAdd += 24 * 60;
			else if(q.q == 4) minAdd += 36 * 60;

			for(Play p : q.plays){
				Double time = this.parseTime(p.gameTime) + minAdd;
				Double fromParse = this.parseTime(fromMin);
				Double toParse = this.parseTime(toMin);

				if(fromParse <= time && toParse >= time){
					plays.add(p);
				}
			}
		}
		return plays;
	}

	private Double parseTime(String time){
		//time = 18:20.2
		String[] values = time.split(":");
		int min = Integer.parseInt(values[0]);
		double seconds = Double.parseDouble(values[1]);

		Double secTime = seconds + min * 60;

		return secTime;
	}
}
