package rs.edu.raf.msa.pbp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Play {
        Integer p;
        @JsonProperty("c")
        String gameTime;
        String d;
        String t;
        String atin;
        Integer x;
        Integer y;
        Integer hs;
        Integer id;
        Integer vs;
        String et;




}
