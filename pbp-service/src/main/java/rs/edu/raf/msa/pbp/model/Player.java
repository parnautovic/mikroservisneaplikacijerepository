package rs.edu.raf.msa.pbp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Player {

	@JsonProperty("c")
	String playerName;
	@JsonProperty("f")
	String playerSecondName;
	@JsonProperty("l")
	String playerFirstName;
	
}
